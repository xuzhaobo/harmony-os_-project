import relationalStore from '@ohos.data.relationalStore';
import {CartProduct} from "../viewmodel/CartProduct"

export class CartDBUtil{
  //定义rdbStore，给后面查询、删除、修改、新增使用
  private rdbStore:relationalStore.RdbStore | null = null
  /**
   * 初始化任务数据表
   */
  initTaskDB(context:Context,tableName:string){
    //1.rdb配置
    const config:relationalStore.StoreConfig = {
      name:"cart.db",//数据库文件名
      securityLevel: relationalStore.SecurityLevel.S1 //数据库安全级别
    }
    //2.初始化SQL
    const sql = `CREATE TABLE IF NOT EXISTS ${tableName}(
                    ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    GOODSNAME TEXT NOT NULL,
                    GOODSIMG TEXT NOT NULL,
                    GOODSPRICE TEXT NOT NULL,
                    GOODSNUMBER INTEGER,
                    CHECKED bit)`
    //3.获取rdb
    relationalStore.getRdbStore(context,config,(err,rdbStore)=>{
      if(err){
        console.log("testTag---","获取rdbStore失败")
        return
      }
      //执行SQL，后续的增删改查都是通过rdbStore完成
      rdbStore.executeSql(sql)
      console.log("testTag---",`创建表${tableName}成功`)
      //保存rdbStore后续都使用
      this.rdbStore = rdbStore
    })
  }

  /**
   * 添加任务列表
   * @param name 任务名称
   */
  insert(product:CartProduct,tableName:string):Promise<number>{
    console.log("能不能走到这里");
    //主键让自动生成，默认状态为false, 需要拿到主键,需要指定返回值Promise<number>
    return (this.rdbStore as relationalStore.RdbStore).insert(tableName,
      { GOODSNAME:product.goods_name,
        GOODSIMG:product.goods_big_logo,
        GOODSPRICE:product.goods_price,
        GOODSNUMBER:product.goods_number,
        CHECKED:product.goods_status
      })
  }
  /**
   * 查询任务列表
   */
  async getDBList(column:Array<string>,tableName:string){
    //1.构建查询条件
    let predicates = new relationalStore.RdbPredicates(tableName)
    // predicates.equalTo("ID",1)
    //2.执行查询，指定查询的字段，字段名要和表中保持一致
    let result = await (this.rdbStore as relationalStore.RdbStore).query(predicates,column)
    //3.解析查询结果
    let tasks:CartProduct[] = []
    //3.2.循环遍历结果集，判断是否结果是否遍历到了最后一行
    while (!result.isAtLastRow){//没有到最后一行就继续往后读取
      //指针移动到下一行数据
      result.goToNextRow()
      //根据字段名获取字段index，从而获取字段值
      let id = result.getString(result.getColumnIndex("ID"))
      let goods_name = result.getString(result.getColumnIndex("GOODSNAME"))
      let checked = result.getLong(result.getColumnIndex("CHECKED"))//返回是1或者0，这里需要给转换一下
      const temp = new CartProduct()
      temp._id = id
      temp.goods_name = goods_name
      temp.goods_status = checked?true:false
      tasks.push(temp)
    }
    return tasks
  }

  /**
   * 根据ID更新任务状态
   * @param id
   * @param finished
   */
  updateDBStatus(id:number,finished:boolean,tableName:string):Promise<number>{
    //2.更新的条件
    let predicates = new relationalStore.RdbPredicates(tableName)
    predicates.equalTo("ID",id)
    //3.更新操作
    return (this.rdbStore as relationalStore.RdbStore).update({checked:finished},predicates)
  }

  /**
   * 根据id删除任务
   * @param id
   */
  deleteDBById(id:number,tableName:string):Promise<number>{
    //1.删除的条件
    let predicates = new relationalStore.RdbPredicates(tableName)
    predicates.equalTo("ID",id)
    //3.更新操作
    return (this.rdbStore as relationalStore.RdbStore).delete(predicates)
  }
}

export const cartDBUtil =  new CartDBUtil();